<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->seedUsers();

        $number = rand(1, 1000);
        $email = 'user+seeder' . $number . '@mail.com';
        $user = User::query()->firstOrNew(['email' => $email]);
        $user->password = '12345678';
        $user->name = 'Seeder';
        $user->save();
    }

    public function seedUsers() {
        $email = 'tcodevn@gmail.com';
        $user = User::query()->firstOrNew(['email' => $email]);
        $user->password = '12345678';
        $user->name = 'TcodeVn';
        $user->save();
    }
}
