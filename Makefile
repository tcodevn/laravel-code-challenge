start: run

run:
	./vendor/bin/sail up
shell:
	./vendor/bin/sail shell

stop:
	docker-compose stop
