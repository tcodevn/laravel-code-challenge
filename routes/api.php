<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiAuthController;
use App\Http\Controllers\CustomerController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Routing APIs authenticate user.
 **/

Route::group(['prefix' => 'auth'], function() {
    Route::post('/register', [ApiAuthController::class, 'register'])->name('register');
    Route::post('/login', [ApiAuthController::class, 'login'])->name('login');
    Route::put('/logout', [ApiAuthController::class, 'logout'])->middleware('jwt.verify')->name('logout');
    Route::get('/user-profile', [ApiAuthController::class, 'getProfile'])->middleware('jwt.verify')->name('user-profile');
});

/**
 * Routing APIs customer to work with Stripe
 **/

Route::group(['prefix' => 'customer', 'middleware' => 'jwt.verify'], function() {
    Route::post('/', [CustomerController::class, 'create'])->name('customer.store');
    Route::get('/', [CustomerController::class, 'index'])->name('customer.index');
    Route::get('/{customerId}', [CustomerController::class, 'get'])->name('customer.get');
    Route::put('/{customerId}', [CustomerController::class, 'update'])->name('customer.update');
});
