# LARAVEL PROJECT - Code Challenge

## About

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Synopsis
The project is to demonstrate my understanding on using Git, Composer and Laravel.

## Description
This exercise are your code structure, design pattern, design principal, and testing approach.

## How to init the project on my local

### Prerequisites
1. Install [Docker](http://docker.io) and [Docker Compose](https://docs.docker.com/compose/).
2. Install Composer
3. Install Laravel Sail [link](https://laravel.com/docs/8.x/sail#introduction)

Configuring A Bash Alias
```
alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
```

### Setup Laravel
- on Mac OS environment by following  [documentation](https://laravel.com/docs/8.x#getting-started-on-macos)
```
curl -s "https://laravel.build/laravel-app" | bash
```

### Install the Cashier package for Stripe using the Composer package manager:
[Cashier package document](https://laravel.com/docs/8.x/billing#installation)
```
composer require laravel/cashier
```

## Run inside Docker Mac OS
```
cd laravel-app
composer install
./vendor/bin/sail up
(or 'make run')
```

Access to laravel container:
```
./vendor/bin/sail shell
(or 'make shell')
```
Run the migration for setup Stripe tables:
```
cp .env.example .env
(Update database connection)
php artisan jwt:secret
php artisan migrate
php artisan db:seed
```

## Run Serving Laravel:
Need PHP version ^7.4 / 8
```
composer install
cp .env.example .env
(Update database connection)
php artisan jwt:secret
php artisan migrate
php artisan db:seed
php artisan serve
```
The domain url: localhost:8080

STRIPE_KEY= your key
STRIPE_SECRET= your secret

Testing Card Credential
Card Name: Test // any other name you can use here
Card Number: 4242424242424242
Month: Any Future Month
Year: Any Future Year
CVV: 123


# Results after running project
## API routes:
        | POST     | api/auth/login            | login            | App\Http\Controllers\ApiAuthController@login                           | api                                               |
        | PUT      | api/auth/logout           | logout           | App\Http\Controllers\ApiAuthController@logout                          | api                                               |
        |          |                           |                  |                                                                        | App\Http\Middleware\JwtMiddleware                 |
        | POST     | api/auth/register         | register         | App\Http\Controllers\ApiAuthController@register                        | api                                               |
        | GET|HEAD | api/auth/user-profile     | user-profile     | App\Http\Controllers\ApiAuthController@getProfile                      | api                                               |
        |          |                           |                  |                                                                        | App\Http\Middleware\JwtMiddleware                 |
        | POST     | api/customer              | customer.store   | App\Http\Controllers\CustomerController@create                         | api                                               |
        |          |                           |                  |                                                                        | App\Http\Middleware\JwtMiddleware                 |
        | GET|HEAD | api/customer              | customer.index   | App\Http\Controllers\CustomerController@index                          | api                                               |
        |          |                           |                  |                                                                        | App\Http\Middleware\JwtMiddleware                 |
        | GET|HEAD | api/customer/{customerId} | customer.get     | App\Http\Controllers\CustomerController@get                            | api                                               |
        |          |                           |                  |                                                                        | App\Http\Middleware\JwtMiddleware                 |
        | PUT      | api/customer/{customerId} | customer.update  | App\Http\Controllers\CustomerController@update                         | api                                               |
        |          |                           |                  |                                                                        | App\Http\Middleware\JwtMiddleware                 |

## Postman Collection:
Exported files saved in the folder "Artifacts/Postman"

## Log files:
Stored in folder 'storage/logs/user-customers.log'

## Unit test:
Stored in folder 'test/Unit/UserCustomerTest.php'

## Some screenshots result of current project:
1. Created customer in Stripe Dashboard:
    https://www.screencast.com/t/Va4MyHEMr
2. Login API user and Login web page:
   https://www.screencast.com/t/5x4AzIEalO
   
