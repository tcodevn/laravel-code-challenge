<?php

namespace App\Repositories;

use App\Helpers\LogToChannels;
use Stripe;
use AwesIO\Repository\Eloquent\BaseRepository;
use App\Models\User;

/**
 * Class UserRepositoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CustomerRepository extends BaseRepository
{
    private const LOG_CHANNEL = 'user-customers';

    protected $stripeClient;

    /**
     * Log helpers.
     *
     * @var LogToChannels
     */
    public $logger;

    /**
     * UserAccountService constructor.
     *
     * @param LogToChannels $logToChannels Log channel helpers
     */

    public function __construct(LogToChannels $logToChannels)
    {
        parent::__construct();

        $this->stripeClient = new Stripe\StripeClient(env('STRIPE_SECRET'));
        $this->logger = $logToChannels;
    }

    public function entity()
    {
        return User::class;
    }

    /**
     * Create customer Stripe.
     *
     * @param array $data
     * @param User $user
     * @return Stripe\Customer
     * @throws Stripe\Exception\ApiErrorException
     */
    public function createCustomer(Array $data, User $user)
    {
        $this->logger->info(
            "Create customer stripe link [user_id: $user->id]",
            static::LOG_CHANNEL,
            $data
        );

        return $this->stripeClient->customers->create([
            'name'        => $data['name'] ?? null,
            'email'       => $data['email'] ?? null,
            'description' => $data['description'] ?? null,
        ]);
    }

    /**
     * @return Stripe\Collection
     * @throws Stripe\Exception\ApiErrorException
     */
    public function getAll()
    {
        return $this->stripeClient->customers->all();
    }

    /**
     * @param $customerId
     * @return Stripe\Customer
     * @throws Stripe\Exception\ApiErrorException
     */
    public function getCustomerDetail($customerId)
    {
        return $this->stripeClient->customers->retrieve($customerId, []);
    }

    /**
     * @param $customerId
     * @param $data
     * @return Stripe\Customer
     * @throws Stripe\Exception\ApiErrorException
     */
    public function updateCustomer($customerId, $data)
    {
        $this->logger->info(
            "Update customer stripe [stripe id: $customerId]",
            static::LOG_CHANNEL,
            $data
        );

        return $this->stripeClient->customers->update($customerId, [
                'name'        => $data['name'] ?? null,
                'email'       => $data['email'] ?? null,
                'description' => $data['description'] ?? null
            ]
        );
    }
}
