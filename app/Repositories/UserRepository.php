<?php

namespace App\Repositories;

use Stripe;
use AwesIO\Repository\Eloquent\BaseRepository;
use App\Models\User;

/**
 * Class UserRepositoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRepository extends BaseRepository
{
    public function entity()
    {
        return User::class;
    }
}
