<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Eloquent model repository. Manages stored entities.
 */
class Repository implements IRepository
{
    /**
     * FQN model name of the repository. Must be determined in the inheritors.
     *
     * @var string
     */
    protected $modelClass;

    /**
     * List of fields, allowed to use in the search
     *
     * Should be determine in the inheritors. Determines the result of the list request of entities.
     *
     * @var array
     */
    protected $searchableFields = [];

    /** {@inheritdoc} */
    public function getSearchableFields(): array
    {
        return $this->searchableFields;
    }

    public function getModelValidationRules(): array
    {
        // TODO: Implement getModelValidationRules() method.
    }

    public function findOrFail($id): Model
    {
        // TODO: Implement findOrFail() method.
    }

    public function create(Model $entity): Model
    {
        // TODO: Implement create() method.
    }

    public function save(Model $entity): Model
    {
        // TODO: Implement save() method.
    }

    public function delete(Model $entity): void
    {
        // TODO: Implement delete() method.
    }

    public function getWhere(array $fieldValues): Collection
    {
        // TODO: Implement getWhere() method.
    }

    public function get(): Collection
    {
        // TODO: Implement get() method.
    }

    public function count(array $fieldValues = []): int
    {
        // TODO: Implement count() method.
    }
}
