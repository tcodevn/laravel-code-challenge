<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Generic model repository contract - manages stored entities.
 */
interface IRepository
{
    /**
     * Returns rules to validate model which managed by this repository.
     *
     * @return array
     */
    public function getModelValidationRules(): array;

    /**
     * Find model by their id.
     *
     * @param string|int $id Model id to find
     *
     * @return Model
     *
     */
    public function findOrFail($id): Model;

    /**
     * Returns first model matching given filters.
     *
     * @param array $fieldValues Filters collection
     *
     * @return Model|null
     *
    public function findWhere(array $fieldValues): ?Model;

    /**
     * Create model in storage.
     *
     * @param Model $entity Model to create
     *
     * @return Model
     */
    public function create(Model $entity): Model;

    /**
     * Save model in storage.
     *
     * @param Model $entity Model to save
     *
     * @return Model
     */
    public function save(Model $entity): Model;

    /**
     * Delete model in storage.
     *
     * @param Model $entity Model to delete
     *
     * @return void
     */
    public function delete(Model $entity): void;

    /**
     * Returns models list.
     *
     * @return Collection|Model[]
     */
    public function get(): Collection;

    /**
     * Returns models list matching given filters.
     *
     * @param array $fieldValues Filters collection
     *
     * @return Collection|Model[]
     */
    public function getWhere(array $fieldValues): Collection;

    /**
     * Return entities count.
     *
     * @param array $fieldValues Conditions that retrieved entities should satisfy
     *
     * @return integer
     */
    public function count(array $fieldValues = []): int;

    /**
     * List of fields, allowed to use in the search.
     * Should be determine in the inheritors. Determines the result of the list request of entities.
     *
     * @return array
     */
    public function getSearchableFields(): array;
}
