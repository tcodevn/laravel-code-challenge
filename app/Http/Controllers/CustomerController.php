<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories\CustomerRepository;
use Illuminate\Support\Facades\Validator;
use Exception;

class CustomerController extends Controller
{
    protected $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function create(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'description' => 'string',
            'name'     => 'string|required',
            'email'    => 'email|required|unique:users,email',
            'password' => 'min:8|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {
            /** @var User $user */
           $user = $this->customerRepository->create([
                'name' => $request->name,
                'description' => $request->description,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);

            $data = [
                'email' => $user->email,
                'name' => $user->name,
                'description' => $request->description,
            ];

            $customer = $this->customerRepository->createCustomer($data, $user);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'There is error when creating customer!'
            ], Response::HTTP_BAD_REQUEST);
        }

        return response()->json([
            'message' => 'Create customer successfully.',
            'data'    => $customer
        ]);
    }

    public function index()
    {
        try {
            $customers = $this->customerRepository->getAll();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'There is error when getting all customers!'
            ], Response::HTTP_BAD_REQUEST);
        }

        return response()->json([
            'message' => 'Get all customers successfully.',
            'data'    => $customers
        ]);
    }

    public function get($customerId)
    {
        if (!$customer = $this->customerRepository->getCustomerDetail($customerId)) {
            return response()->json([
                'error' => 'Customer is not found!'
            ], Response::HTTP_BAD_REQUEST);
        }

        return response()->json([
            'message' => 'Get customer successfully.',
            'data'    => $customer
        ]);
    }

    public function update(Request $request, $customerId)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string',
            'email'  => 'email',
            'description' => 'string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        try {
            if ($this->customerRepository->getCustomerDetail($customerId)) {
                $this->customerRepository->getCustomerDetail($customerId, $validator->validated());
            } else {
                return response()->json([
                    'error' => 'The customer is not found',
                ], Response::HTTP_BAD_REQUEST);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'There is error when updating customer!'
            ], Response::HTTP_BAD_REQUEST);
        }

        return response()->json([
            'message' => 'Update customer successfully.',
            'data'    => $this->customerRepository->getCustomerDetail($customerId)
        ]);
    }
}
