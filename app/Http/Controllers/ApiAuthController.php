<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories\UserRepository;
use App\Http\Transformers\UserTransformer;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;

class ApiAuthController extends Controller
{
    protected $userRepository;
    protected $userTransformer;

    public function __construct(UserRepository $userRepository, UserTransformer $userTransformer)
    {
        $this->userRepository = $userRepository;
        $this->userTransformer = $userTransformer;
    }

    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'string|required',
            'email'    => 'email|required|unique:users,email',
            'password' => 'min:8|required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {
            $user = $this->userRepository->create(array_merge(
                $validator->validated(),
                ['password' => bcrypt($request->password)]
            ));
        } catch (Exception $e) {
            return response()->json([
                'error' => 'There is error when registering account!'
            ]);
        }

        return response()->json([
            'message' => 'Create account successfully.',
            'data'    => $this->userTransformer->transform($user)
        ]);
    }

    public function login(Request $request): JsonResponse
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user = $this->userRepository->findWhere([ 'email' => $input['email']]);
        if (!$user) {
            return response()->json([
                'error' => 'Your email is incorrect!'
            ], Response::HTTP_UNAUTHORIZED);
        }

        if (!$jwtToken = JWTAuth::attempt(['email' => $input['email'], 'password' => $input['password']])) {
            return response()->json([
                'error' => 'Your email and/or password are incorrect.'
            ], Response::HTTP_UNAUTHORIZED);
        }

        $user = auth()->user();

        return response()->json([
            'token'   => $jwtToken,
            'message' => 'Logged in successfully.',
            'data'    => $this->userTransformer->transform($user),
        ]);
    }

    public function getProfile(Request $request)
    {
        if ($id = auth()->user()->id) {
            return response()->json([
                'message' => "Get user profile successfully.",
                'data' => $this->userTransformer->transform($this->userRepository->findOrFail($id))
            ]);
        }

        return response()->json([
            'error' => 'Invalid Request!'
        ], Response::HTTP_BAD_REQUEST);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json([
            'message' => 'User successfully signed out'
        ]);
    }
}
