<?php

namespace App\Http\Transformers;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Transformer user data by add more fields.
 */
class UserTransformer extends BaseTransformer
{
    /**
     * {@inheritdoc}
     */
    public function transform(Arrayable $model): array
    {
        $data = parent::transform($model);

        return $data;
    }
}
