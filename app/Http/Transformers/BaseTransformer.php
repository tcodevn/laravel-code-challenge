<?php

namespace App\Http\Transformers;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;

/**
 * Converts model to raw array.
 * If model has date fields, converts them to ISO 8601 format
 */
class BaseTransformer implements IDataTransformer
{
    /**
     * Transform model into array
     *
     * @param Arrayable $model Model to be transformed
     *
     * @return array
     */
    public function transform(Arrayable $model): array
    {
        $result = $model->toArray();
        if ($model instanceof Model) {
            $result = $this->removeHiddenFields($result, $model);
        }

        return $result;
    }

    /**
     * Force remove hidden fields.
     * For some reason toArray() may ignore $visible and $hidden Eloquent fields
     *
     * @param array $result model, serialized to array
     * @param Model $model original Eloquent model
     *
     * @return array
     */
    protected function removeHiddenFields(array $result, Model $model): array
    {
        $hiddenFields = $model->getHidden();
        if ($hiddenFields && count($hiddenFields) > 0) {
            foreach ($hiddenFields as $field) {
                if (array_key_exists($field, $result)) {
                    unset($result[$field]);
                }
            }
        }

        // If Eloquent model has both $hidden and $visible arrays then need to satisfy both restrictions
        $visibleFields = $model->getVisible();
        if ($visibleFields && count($visibleFields) > 0) {
            foreach ($result as $field => $value) {
                if (!in_array($field, $visibleFields)) {
                    unset($result[$field]);
                }
            }
        }

        return $result;
    }
}
