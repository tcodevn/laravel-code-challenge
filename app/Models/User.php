<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Cashier\Billable;
use App\Services\Stripe\Concerns\HasStripeAccountContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Application User.
 *
 * @property int $id User identifier
 * @property string $name User name
 * @property string $email User email address
 * @property string $password User encoded password
 */
class User extends Authenticatable implements HasStripeAccountContract, JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, Billable;

    public const TABLE = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'description',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getStripeAccountId(): ?string
    {
        // TODO: Implement getStripeAccountId() method.
    }

    public function setStripeAccountId(string $value): void
    {
        // TODO: Implement setStripeAccountId() method.
    }

    public function hasStripeAccountId(): bool
    {
        // TODO: Implement hasStripeAccountId() method.
    }

    public function getStripeAccountMetadata(): array
    {
        // TODO: Implement getStripeAccountMetadata() method.
    }

    public function getJWTIdentifier()
    {
        // TODO: Implement getJWTIdentifier() method.
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        // TODO: Implement getJWTCustomClaims() method.
        return [];
    }
}
