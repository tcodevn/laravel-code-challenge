<?php

namespace App\Services\UserAccounts;

use App\Dto\Withdrawal\AccountVerificationDto;
use App\Dto\Withdrawal\BankAccountCreateDto;
use App\Exceptions\Withdrawal\BankAccountAlreadyExistedException;
use App\Exceptions\Withdrawal\NotSupportedUserException;
use App\Exceptions\Withdrawal\RetrieveAccountFailedException;
use App\Exceptions\Withdrawal\StripeAccountRequiredException;
use App\Exceptions\Withdrawal\UserAccountAlreadyExistedException;
use App\Helpers\LogToChannels;
use App\Models\User;
use App\Services\Stripe\Accounts\AccountCapabilities;
use App\Services\Stripe\Accounts\AccountService;
use App\Services\Stripe\Accounts\IAccountService;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Stripe\Account as StripeAccount;
use Stripe\AccountLink;
use Stripe\BankAccount;
use Stripe\Exception\ApiErrorException as StripeApiException;

/**
 * Handle business logic for user accounts.
 */
class UserAccountService implements IUserAccountService
{
    private const LOG_CHANNEL = 'user-accounts';
    private const DEFAULT_COUNTRY = 'US';
    private const TOS_ACCEPTANCE_DATE = 'tos_acceptance.date';
    private const TOS_ACCEPTANCE_IP = 'tos_acceptance.ip';
    private const EXTERNAL_ACCOUNT = 'external_account';

    /**
     * Stripe account service.
     *
     * @var IAccountService
     */
    protected $accountService;

    /**
     * Log helpers.
     *
     * @var LogToChannels
     */
    public $logger;

    /**
     * UserAccountService constructor.
     *
     * @param IAccountService $accountService Stripe account service
     * @param LogToChannels $logToChannels Log channel helpers
     */
    public function __construct(
        IAccountService $accountService,
        LogToChannels $logToChannels
    ) {
        $this->accountService = $accountService;
        $this->logger = $logToChannels;
    }

    /**
     * Retrieve or create Stripe account for given user.
     *
     * {@inheritDoc}
     */
    public function retrieveOrCreateFor(User $user): StripeAccount
    {
        $this->verifySupportedUser($user);

        if (!$user->hasStripeAccountId()) {
            $this->createFor($user);
        }

        return $this->retrieve($user);
    }

    /**
     * Create Stripe account for given user.
     *
     * {@inheritDoc}
     */
    public function createFor(User $user, ?string $acceptedAgreementsIP = null): StripeAccount
    {
        $this->verifySupportedUser($user);
        $this->verifyUserAccountNotExisted($user);

        // Create "custom" account with "transfers" capability only.
        $account = $this->accountService->create(
            (new AccountCapabilities())->enableTransfers(),
            StripeAccount::TYPE_CUSTOM,
            self::DEFAULT_COUNTRY,
            StripeAccount::BUSINESS_TYPE_INDIVIDUAL,
            $acceptedAgreementsIP,
            null,
            $user->getStripeAccountMetadata()
        );

        // Store Stripe account ID for user.
        $user->setStripeAccountId($account->id);
        $user->save();

        return $account;
    }

    /**
     * Retrieve account for given user.
     *
     * {@inheritDoc}
     */
    public function retrieve(User $user): ?StripeAccount
    {
        return $user->hasStripeAccountId() ? $this->accountService->retrieve($user->getStripeAccountId()) : null;
    }

    /**
     * Get account verification info for given user.
     *
     * {@inheritDoc}
     */
    public function getAccountVerification(User $user, bool $includeBankAccount = true): AccountVerificationDto
    {
        $this->verifySupportedUser($user);
        if (!$user->hasStripeAccountId()) {
            return new AccountVerificationDto([]);
        }

        try {
            $stripeAccount = $this->retrieve($user);
            $bankAccount = $includeBankAccount && $user->stripe_bank_account_id
                ? $this->accountService->retrieveExternalBankAccount(
                    $user->stripe_account_id,
                    $user->stripe_bank_account_id
                )
                : null;

            return $this->prepareAccountVerification($stripeAccount, $bankAccount);
        } catch (StripeApiException $e) {
            $this->logger->error($e->getMessage(), static::LOG_CHANNEL);
            throw new RetrieveAccountFailedException(trans('withdrawal.receive_account_failed'));
        }
    }

    /**
     * Create account link for given user.
     *
     * {@inheritDoc}
     */
    public function createAccountLink(User $user): AccountLink
    {
        $this->verifySupportedUser($user);
        $this->requireStripeAccount($user);

        $failureUrl = $this->generateAccountLinkFailureUrl();
        $successUrl = $this->getAccountLinkSuccessUrl();

        try {
            $accountLink = $this->accountService->createAccountLink(
                $user->stripe_account_id,
                $failureUrl,
                $successUrl,
                AccountService::CUSTOM_ACCOUNT_VERIFICATION
            );
            $this->logger->info(
                "Create account link [user_id:$user->id]",
                static::LOG_CHANNEL,
                $accountLink->toArray()
            );

            return $accountLink;
        } catch (StripeApiException $e) {
            $this->logger->error($e->getMessage(), static::LOG_CHANNEL);
            throw $e;
        }
    }

    /**
     * Generate account link failure url.
     *
     * @return string
     */
    protected function generateAccountLinkFailureUrl(): string
    {
        return sprintf("%s?rid=%s", config('stripe.account_link_failure_url'), Str::uuid()->toString());
    }

    /**
     * Returns account link success url.
     *
     * @return string
     */
    protected function getAccountLinkSuccessUrl(): string
    {
        return config('stripe.account_link_success_url');
    }

    /**
     * Prepare account verification info.
     *
     * @param StripeAccount $account Stripe account
     * @param BankAccount|null $bankAccount Stripe bank account
     *
     * @return AccountVerificationDto
     */
    protected function prepareAccountVerification(
        StripeAccount $account,
        ?BankAccount $bankAccount
    ): AccountVerificationDto {
        $accountVerification = new AccountVerificationDto([]);
        $accountVerification->account = $account;
        $accountVerification->bank_account = $bankAccount;
        $accountVerification->payout_enabled = $account->payouts_enabled;
        /**
         * requirements.eventually_due which contains fields that need to be collected
         * assuming all volume thresholds are reached.
         * As they become required, these fields appear in currently_due as well, and the current_deadline is set.
         *
         * @link https://stripe.com/docs/api/accounts/object#account_object-requirements-eventually_due
         */
        $eventuallyDue = collect(Arr::get($account->requirements->toArray(), AccountService::EVENTUALLY_DUE, []));
        $accountVerification->agreements_accepted = $this->getAgreementsAcceptedStatus($eventuallyDue);
        $accountVerification->account_verification_required =
            $this->getAccountVerificationRequiredStatus($eventuallyDue);

        return $accountVerification;
    }

    /**
     * Get agreements accepted status.
     *
     * @param Collection $eventuallyDue Eventually due of Stripe account
     *
     * @return boolean
     */
    protected function getAgreementsAcceptedStatus(Collection $eventuallyDue): bool
    {
        return $eventuallyDue->intersect(collect([self::TOS_ACCEPTANCE_DATE, self::TOS_ACCEPTANCE_IP]))
                             ->isEmpty();
    }

    /**
     * Get account verification required status.
     *
     * @param Collection $eventuallyDue Eventually due of Stripe account
     *
     * @return boolean
     */
    protected function getAccountVerificationRequiredStatus(Collection $eventuallyDue): bool
    {
        $nonAccountVerificationFields = collect([
            self::TOS_ACCEPTANCE_DATE,
            self::TOS_ACCEPTANCE_IP,
            self::EXTERNAL_ACCOUNT,
        ]);

        return $eventuallyDue->diff($nonAccountVerificationFields)
                             ->isNotEmpty();
    }

    /**
     * Verify if user is supported for withdrawal process.
     *
     * {@inheritDoc}
     */
    public function verifySupportedUser(User $user): void
    {
        if ($user->isAdmin()) {
            throw new NotSupportedUserException($user);
        }
    }

    /**
     * Ensures user has Stripe account.
     *
     * {@inheritDoc}
     */
    public function requireStripeAccount(User $user): void
    {
        if (!$user->hasStripeAccountId()) {
            throw new StripeAccountRequiredException();
        }
    }

    /**
     * Create bank account for user.
     *
     * {@inheritDoc}
     */
    public function createBankAccount(User $user, BankAccountCreateDto $bankAccountCreateDto): BankAccount
    {
        $this->verifySupportedUser($user);
        $this->requireStripeAccount($user);

        try {
            $bankAccount = $this->accountService->createExternalBankAccount(
                $user->stripe_account_id,
                $bankAccountCreateDto->toArray(),
                ['user_id' => $user->id]
            );

            $this->logger->info(
                "Create bank account [user_id:$user->id]",
                static::LOG_CHANNEL,
                $bankAccount->toArray()
            );

            // Store bank account ID for user.
            $user->stripe_bank_account_id = $bankAccount->id;
            $user->save();

            return $bankAccount;
        } catch (StripeApiException $e) {
            $this->logger->error($e->getMessage(), static::LOG_CHANNEL);
            throw $e;
        }
    }

    /**
     * Verify if user have no user account.
     *
     * {@inheritDoc}
     */
    public function verifyUserAccountNotExisted(User $user): void
    {
        if ($user->stripe_account_id) {
            throw new UserAccountAlreadyExistedException();
        }
    }

    /**
     * Verify if user is already has bank account.
     *
     * {@inheritDoc}
     */
    public function verifyBankAccountExisted(User $user): void
    {
        if ($user->stripe_bank_account_id) {
            throw new BankAccountAlreadyExistedException();
        }
    }

    /**
     * Accept agreements and create Stripe account if not exist.
     *
     * {@inheritDoc}
     */
    public function acceptAgreements(User $user, string $acceptedAgreementsIP): StripeAccount
    {
        $this->verifySupportedUser($user);

        if ($user->hasStripeAccountId()) {
            return $this->retrieve($user);
        } else {
            return $this->createFor($user, $acceptedAgreementsIP);
        }
    }
}
