<?php

namespace App\Services\UserAccounts;

use App\Dto\Withdrawal\AccountVerificationDto;
use App\Dto\Withdrawal\BankAccountCreateDto;
use App\Exceptions\Withdrawal\StripeAccountRequiredException;
use App\Models\User;
use Stripe\Account as StripeAccount;
use Stripe\AccountLink;
use Stripe\BankAccount;
use Stripe\Exception\ApiErrorException as StripeApiException;

/**
 * Handle business logic for user accounts.
 */
interface IUserAccountService
{
    /**
     * Retrieve or create Stripe account for given user.
     *
     * @param User $user User model
     *
     * @return StripeAccount
     *
     * @throws StripeApiException
     */
    public function retrieveOrCreateFor(User $user): StripeAccount;

    /**
     * Create Stripe account for given user.
     *
     * @param User $user User model
     * @param string|null $acceptedAgreementsIP User’s IP address, specified if user accepted agreements
     *
     * @return StripeAccount
     *
     * @throws StripeApiException
     */
    public function createFor(User $user, ?string $acceptedAgreementsIP = null): StripeAccount;

    /**
     * Retrieve account for given user.
     *
     * @param User $user User model
     *
     * @return StripeAccount|null
     *
     * @throws StripeApiException
     */
    public function retrieve(User $user): ?StripeAccount;

    /**
     * Get account verification info for given user.
     *
     * @param User $user User model
     * @param boolean $includeBankAccount Determines if include bank account info
     *
     * @return AccountVerificationDto
     */
    public function getAccountVerification(User $user, bool $includeBankAccount = true): AccountVerificationDto;

    /**
     * Create account link for given user.
     *
     * @param User $user User model
     *
     * @return AccountLink
     */
    public function createAccountLink(User $user): AccountLink;

    /**
     * Create bank account for user.
     *
     * @param User $user User model
     * @param BankAccountCreateDto $bankAccountCreateDto Bank account creation data
     *
     * @return BankAccount
     * @throws StripeApiException
     */
    public function createBankAccount(User $user, BankAccountCreateDto $bankAccountCreateDto): BankAccount;

    /**
     * Verify if user is support for withdrawal process.
     *
     * @param User $user User model
     *
     * @return void
     */
    public function verifySupportedUser(User $user): void;

    /**
     * Ensures user has Stripe account.
     *
     * @param User $user User model
     *
     * @return void
     *
     * @throws StripeAccountRequiredException
     */
    public function requireStripeAccount(User $user): void;

    /**
     * Accept agreements and create Stripe account if not exist.
     *
     * @param User $user User model
     * @param string|null $acceptedAgreementsIP User’s IP address, specified if user accepted agreements
     *
     * @return StripeAccount
     *
     * @throws StripeApiException
     */
    public function acceptAgreements(User $user, string $acceptedAgreementsIP): StripeAccount;
}
