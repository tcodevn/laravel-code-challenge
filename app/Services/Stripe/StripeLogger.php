<?php

namespace App\Services\Stripe;

use App\Services\Stripe\Concerns\IStripeLogger;
use Monolog\Logger;

/**
 * Logger for stripe api services.
 */
class StripeLogger extends Logger implements IStripeLogger
{
}
