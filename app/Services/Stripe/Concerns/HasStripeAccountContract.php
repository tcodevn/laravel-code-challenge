<?php

namespace App\Services\Stripe\Concerns;

/**
 * Provide methods for interacts with stripe account in model.
 */
interface HasStripeAccountContract
{
    /**
     * Returns Stripe account ID.
     *
     * @return string|null
     */
    public function getStripeAccountId(): ?string;

    /**
     * Set Stripe account ID.
     *
     * @param string $value Value
     *
     * @return void
     */
    public function setStripeAccountId(string $value): void;

    /**
     * Determine if the entity has a Stripe account ID.
     *
     * @return boolean
     */
    public function hasStripeAccountId(): bool;

    /**
     * Returns metadata should attach to Stripe account.
     *
     * @return mixed[]|array
     */
    public function getStripeAccountMetadata(): array;
}
