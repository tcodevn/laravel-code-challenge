<?php

namespace App\Services\Stripe\Concerns;

use Psr\Log\LoggerInterface;

/**
 * Interface for all Stripe api services.
 */
interface IStripeService
{
    /**
     * Return the API key used for requests.
     *
     * @return string
     */
    public function getApiKey(): ?string;

    /**
     * Set API key.
     *
     * @param string|null $apiKey The API key
     *
     * @return void
     */
    public function setApiKey(?string $apiKey = null): void;

    /**
     * Returns the API version used for requests, null if we're using the latest version.
     *
     * @return string
     */
    public function getApiVersion(): ?string;

    /**
     * Set API version.
     *
     * @param string|null $apiVersion The API version
     *
     * @return void
     */
    public function setApiVersion(?string $apiVersion = null): void;

    /**
     * Get logger.
     *
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface;

    /**
     * Set logger.
     *
     * @param LoggerInterface $logger Logger
     *
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void;

    /**
     * Get Stripe options.
     *
     * @return string[]|array
     */
    public function getStripeOptions(): array;

    /**
     * Returns additional metadata (that will be added automatically to Stripe requests).
     *
     * @return mixed[]|array
     */
    public function getAdditionalMetadata(): array;
}
