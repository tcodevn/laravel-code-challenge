<?php

namespace App\Services\Stripe\Concerns;

use Psr\Log\LoggerInterface;
use Stripe\Util\LoggerInterface as StripeLoggerInterface;

/**
 * Logger for stripe api services.
 */
interface IStripeLogger extends LoggerInterface, StripeLoggerInterface
{
}
