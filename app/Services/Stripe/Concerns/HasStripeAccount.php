<?php

namespace App\Services\Stripe\Concerns;

use Stripe\Account as StripeAccount;
use Stripe\Exception\ApiErrorException as StripeApiException;

/**
 * Implement methods for interacts with stripe account in model.
 *
 * @property string $stripe_account_id Stripe account id
 */
trait HasStripeAccount
{
    /**
     * Returns Stripe account ID.
     *
     * @return string|null
     */
    public function getStripeAccountId(): ?string
    {
        return $this->stripe_account_id;
    }

    /**
     * Set Stripe account ID.
     *
     * @param string $value Value
     *
     * @return void
     */
    public function setStripeAccountId(string $value): void
    {
        $this->stripe_account_id = $value;
    }

    /**
     * Determine if the entity has a Stripe account ID.
     *
     * @return boolean
     */
    public function hasStripeAccountId(): bool
    {
        return !empty($this->getStripeAccountId());
    }

    /**
     * Get the Stripe account for the model.
     *
     * @return StripeAccount|null
     *
     * @throws StripeApiException
     */
    public function asStripeAccount(): ?StripeAccount
    {
        return StripeAccount::retrieve($this->getStripeAccountId());
    }

    /**
     * Returns metadata should attach to Stripe account.
     *
     * @return mixed[]|array
     */
    public function getStripeAccountMetadata(): array
    {
        return [];
    }
}
