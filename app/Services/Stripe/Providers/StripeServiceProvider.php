<?php

namespace App\Services\Stripe\Providers;

use App\Services\Stripe\Accounts\AccountService;
use App\Services\Stripe\Accounts\IAccountService;
use App\Services\Stripe\Concerns\IStripeLogger;
use App\Services\Stripe\Payouts\IPayoutService;
use App\Services\Stripe\Payouts\PayoutService;
use App\Services\Stripe\StripeLogger;
use App\Services\Stripe\Transfers\ITransferService;
use App\Services\Stripe\Transfers\TransferService;
use Closure;
use Illuminate\Support\ServiceProvider;
use InvalidArgumentException;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\StreamHandler;
use Stripe\Stripe;

/**
 * Registers stripe api services.
 */
class StripeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->registerStripeConfigurations();
        $this->registerLogger();
        $this->registerServices();
    }

    /**
     * Register bindings for logger.
     *
     * @return void
     */
    protected function registerLogger(): void
    {
        $this->app->bind(IStripeLogger::class, StripeLogger::class);
        $this->app->bind(StripeLogger::class, Closure::fromCallable([$this, 'createStripeLogger']));
    }

    /**
     * Create stripe logger with default configuration.
     *
     * @return StripeLogger
     */
    protected function createStripeLogger(): StripeLogger
    {
        return new StripeLogger('stripe');
    }

    /**
     * Create log handler.
     *
     * @param string $logFile Log file path
     *
     * @return HandlerInterface
     */
    protected function createLogHandler(string $logFile): HandlerInterface
    {
        if (!is_writable($logFile)) {
            throw new InvalidArgumentException(
                "The stream or file \"$logFile\" could not be opened. Please correct \"stripe.log_file\" configuration"
            );
        }

        $handler = new StreamHandler($logFile);
        $formatter = tap(
            new LineFormatter(null, 'Y-m-d H:i:s', true, true),
            function (LineFormatter $formatter): void {
                $formatter->includeStacktraces();
            }
        );

        return $handler->setFormatter($formatter);
    }

    /**
     * Register bindings for logger.
     *
     * @return void
     */
    protected function registerStripeConfigurations(): void
    {
        Stripe::setApiKey(config('stripe.api_key'));
        $apiVersion = config('stripe.api_version');
        if ($apiVersion) {
            Stripe::setApiVersion(config('stripe.api_version'));
        }
    }

    /**
     * Register bindings for services.
     *
     * @return void
     */
    protected function registerServices(): void
    {
        $this->app->bind(IAccountService::class, AccountService::class);
        $this->app->bind(IPayoutService::class, PayoutService::class);
        $this->app->bind(ITransferService::class, TransferService::class);
    }
}
