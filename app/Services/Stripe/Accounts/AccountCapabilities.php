<?php

namespace App\Services\Stripe\Accounts;

use Illuminate\Contracts\Support\Arrayable;
use Stripe\Account as StripeAccount;

/**
 * Represents for Stripe account capabilities.
 */
class AccountCapabilities implements Arrayable
{

    /**
     * Contains enabled capabilities for instance.
     *
     * @var string[]
     */
    protected $enables;

    /**
     * Get the instance as an array.
     *
     * @return array|string[]
     */
    public function toArray(): array
    {
        return array_keys($this->enables);
    }

    /**
     * Enable "card_payments" capability.
     *
     * @return AccountCapabilities
     */
    public function enableCardPayments(): AccountCapabilities
    {
        $this->enables[StripeAccount::CAPABILITY_CARD_PAYMENTS] = true;

        return $this;
    }

    /**
     * Disable "card_payments" capability.
     *
     * @return AccountCapabilities
     */
    public function disableCardPayments(): AccountCapabilities
    {
        unset($this->enables[StripeAccount::CAPABILITY_CARD_PAYMENTS]);

        return $this;
    }

    /**
     * Enable "transfers" capability.
     *
     * @return AccountCapabilities
     */
    public function enableTransfers(): AccountCapabilities
    {
        $this->enables[StripeAccount::CAPABILITY_TRANSFERS] = true;

        return $this;
    }

    /**
     * Disable "transfers" capability.
     *
     * @return AccountCapabilities
     */
    public function disableTransfers(): AccountCapabilities
    {
        unset($this->enables[StripeAccount::CAPABILITY_TRANSFERS]);

        return $this;
    }
}
