<?php

namespace App\Services\Stripe\Accounts;

use App\Services\Stripe\Concerns\IStripeService;
use Stripe\Account as StripeAccount;
use Stripe\AccountLink;
use Stripe\BankAccount;
use Stripe\Exception\ApiErrorException as StripeApiException;

/**
 * Service for management stripe accounts: create account, update account, retrieve account, ...
 */
interface IAccountService extends IStripeService
{
    /**
     * Create Stripe account.
     *
     * @param AccountCapabilities $capabilities Requested capabilities
     * @param string $type Stripe account type
     * @param string $country Country
     * @param string $businessType Business type
     * @param string|null $ip User’s IP address, specified if user accepted agreements
     * @param string|null $email Email
     * @param string[] $metadata Metadata
     *
     * @return StripeAccount
     *
     * @throws StripeApiException
     */
    public function create(
        AccountCapabilities $capabilities,
        string $type = StripeAccount::TYPE_CUSTOM,
        string $country = AccountService::DEFAULT_COUNTRY,
        string $businessType = StripeAccount::BUSINESS_TYPE_INDIVIDUAL,
        ?string $ip = null,
        ?string $email = null,
        array $metadata = []
    ): StripeAccount;

    /**
     * Update account.
     *
     * @param string $id Account ID
     * @param array $payload Account payload
     *
     * @return StripeAccount
     *
     * @throws StripeApiException
     */
    public function update(string $id, array $payload): StripeAccount;

    /**
     * Retrieve account.
     *
     * @param string $id Account ID
     *
     * @return StripeAccount
     *
     * @throws StripeApiException
     */
    public function retrieve(string $id): StripeAccount;

    /**
     * Retrieve platform account.
     *
     * @return StripeAccount
     *
     * @throws StripeApiException
     */
    public function getPlatformAccount(): StripeAccount;

    /**
     * Create account link for account.
     *
     * @param string $accountId Stripe account ID
     * @param string $failureUrl Failure url
     * @param string $successUrl Success url
     * @param string $type Account link type
     * @param string|null $collect Information needs to collect (One of currently_due or eventually_due)
     *
     * @return AccountLink
     *
     * @throws StripeApiException
     */
    public function createAccountLink(
        string $accountId,
        string $failureUrl = AccountService::DEFAULT_FAILURE_URL,
        string $successUrl = AccountService::DEFAULT_SUCCESS_URL,
        string $type = AccountService::CUSTOM_ACCOUNT_VERIFICATION,
        string $collect = AccountService::EVENTUALLY_DUE
    ): AccountLink;

    /**
     * Retrieve external bank account.
     *
     * @param string $accountId Stripe account ID
     * @param string $externalAccountId Stripe external bank account ID
     *
     * @return BankAccount
     *
     * @throws StripeApiException
     */
    public function retrieveExternalBankAccount(string $accountId, string $externalAccountId): BankAccount;

    /**
     * Create external bank account.
     *
     * @param string $accountId Stripe account ID
     * @param array|string[] $bankAccountPayload Bank account payload (country, currency, routing_number,
     *     account_number...)
     * @param array|string[] $metadata Metadata
     *
     * @return BankAccount
     *
     * @throws StripeApiException
     */
    public function createExternalBankAccount(
        string $accountId,
        array $bankAccountPayload,
        array $metadata = []
    ): BankAccount;
}
