<?php

namespace App\Services\Stripe\Accounts;

use App\Services\Stripe\StripeService;
use Stripe\Account as StripeAccount;
use Stripe\AccountLink;
use Stripe\BankAccount;

/**
 * Service for management stripe accounts: create account, update account, retrieve account, ...
 */
class AccountService extends StripeService implements IAccountService
{
    public const ACCOUNT_PREFIX = 'acct_';
    public const ACCOUNT_TYPE = 'type';
    public const BUSINESS_TYPE = 'business_type';
    public const COUNTRY = 'country';
    public const CURRENCY = 'currency';
    public const REQUESTED_CAPABILITIES = 'requested_capabilities';
    public const EMAIL = 'email';
    public const METADATA = 'metadata';
    public const TYPE = 'type';
    public const COLLECT = 'collect';
    public const CUSTOM_ACCOUNT_VERIFICATION = 'custom_account_verification';
    public const SUCCESS_URL = 'success_url';
    public const FAILURE_URL = 'failure_url';
    public const CURRENTLY_DUE = 'currently_due';
    public const EVENTUALLY_DUE = 'eventually_due';
    public const OBJECT = 'object';
    public const EXTERNAL_ACCOUNT = 'external_account';
    public const TOS_ACCEPTANCE = 'tos_acceptance';
    public const DATE = 'date';
    public const IP = 'ip';

    // Default constants.
    public const DEFAULT_COUNTRY = 'US';
    public const DEFAULT_CURRENCY = 'usd';
    public const DEFAULT_SUCCESS_URL = 'https://example.com/success';
    public const DEFAULT_FAILURE_URL = 'https://example.com/failure';

    /**
     * Create Stripe account.
     *
     * {@inheritDoc}
     */
    public function create(
        AccountCapabilities $capabilities,
        string $type = StripeAccount::TYPE_CUSTOM,
        string $country = self::DEFAULT_COUNTRY,
        string $businessType = StripeAccount::BUSINESS_TYPE_INDIVIDUAL,
        ?string $ip = null,
        ?string $email = null,
        array $metadata = []
    ): StripeAccount {
        $payload = $this->buildCreationPayload(
            $capabilities,
            $type,
            $country,
            $businessType,
            $ip,
            $email,
            $metadata
        );

        if ($this->logger) {
            $this->logger->info(sprintf("[%s][Request]", __METHOD__), $payload);
        }

        $account = $this->handleException(function () use ($payload) {
            return StripeAccount::create($payload, $this->getStripeOptions());
        });

        if ($this->logger) {
            $this->logger->info(
                sprintf("[%s][Response][account_id:%s]", __METHOD__, $account->id),
                $account->toArray()
            );
        }

        return $account;
    }

    /**
     * Build account creation payload.
     *
     * @param AccountCapabilities $capabilities Requested capabilities
     * @param string $type Stripe account type
     * @param string $country Country
     * @param string $businessType Business type
     * @param string|null $ip User’s IP address, specified if user accepted agreements
     * @param string|null $email Email
     * @param string[] $metadata Metadata
     *
     * @return array|string[]
     *
     * @link https://stripe.com/docs/api/accounts/create
     */
    protected function buildCreationPayload(
        AccountCapabilities $capabilities,
        string $type = StripeAccount::TYPE_CUSTOM,
        string $country = self::DEFAULT_COUNTRY,
        string $businessType = StripeAccount::BUSINESS_TYPE_INDIVIDUAL,
        ?string $ip = null,
        ?string $email = null,
        array $metadata = []
    ): array {
        $requestedCapabilities = $capabilities ? $capabilities->toArray() : [StripeAccount::CAPABILITY_TRANSFERS];

        $payload = [
            self::ACCOUNT_TYPE => $type,
            self::REQUESTED_CAPABILITIES => $requestedCapabilities,
            self::BUSINESS_TYPE => $businessType,
            self::METADATA => $this->attachAdditionalMetadata($metadata),
        ];
        if ($country) {
            $payload[self::COUNTRY] = $country;
        }
        if ($email) {
            $payload[self::EMAIL] = $email;
        }
        /**
         * Indicates if user accepted services agreements.
         *
         * @link https://stripe.com/docs/connect/updating-accounts#indicating-acceptance-on-their-stripe-account
         */
        if ($ip) {
            $payload[self::TOS_ACCEPTANCE][self::DATE] = time();
            $payload[self::TOS_ACCEPTANCE][self::IP] = $ip;
        }

        return $payload;
    }

    /**
     * Update account.
     *
     * {@inheritDoc}
     */
    public function update(string $id, array $payload): StripeAccount
    {
        if ($this->logger) {
            $this->logger->info(sprintf("[%s][Request][account_id:%s]", __METHOD__, $id), $payload);
        }

        $account = $this->handleException(function () use ($id, $payload) {
            return StripeAccount::update($id, $payload, $this->getStripeOptions());
        });

        if ($this->logger) {
            $this->logger->info(
                sprintf("[%s][Response][account_id:%s]", __METHOD__, $account->id),
                $account->toArray()
            );
        }

        return $account;
    }

    /**
     * Retrieve account.
     *
     * {@inheritDoc}
     */
    public function retrieve(string $id): StripeAccount
    {
        if ($this->logger) {
            $this->logger->info(sprintf("[%s][Request][account_id:%s]", __METHOD__, $id));
        }

        $account = $this->handleException(function () use ($id) {
            return StripeAccount::retrieve($id, $this->getStripeOptions());
        });

        if ($this->logger) {
            $this->logger->info(sprintf("[%s][Response][account_id:%s]", __METHOD__, $id), $account->toArray());
        }

        return $account;
    }

    /**
     * Retrieve platform account.
     *
     * {@inheritDoc}
     */
    public function getPlatformAccount(): StripeAccount
    {
        if ($this->logger) {
            $this->logger->info(sprintf("[%s][Request]", __METHOD__));
        }

        $account = $this->handleException(function () {
            return StripeAccount::retrieve(null, $this->getStripeOptions());
        });

        if ($this->logger) {
            $this->logger->info(sprintf("[%s][Response]", __METHOD__), $account->toArray());
        }

        return $account;
    }

    /**
     * Create account link for account.
     *
     * {@inheritDoc}
     */
    public function createAccountLink(
        string $accountId,
        string $failureUrl = self::DEFAULT_FAILURE_URL,
        string $successUrl = self::DEFAULT_SUCCESS_URL,
        string $type = self::CUSTOM_ACCOUNT_VERIFICATION,
        string $collect = self::EVENTUALLY_DUE
    ): AccountLink {
        $payload = [
            StripeAccount::OBJECT_NAME => $accountId,
            self::FAILURE_URL => $failureUrl,
            self::SUCCESS_URL => $successUrl,
            self::TYPE => $type,
            self::COLLECT => $collect,
        ];

        if ($this->logger) {
            $this->logger->info(sprintf("[%s][Request][account_id:%s]", __METHOD__, $accountId), $payload);
        }

        $accountLink = $this->handleException(function () use ($accountId, $payload) {
            return AccountLink::create($payload, $this->getStripeOptions());
        });

        if ($this->logger) {
            $this->logger->info(
                sprintf("[%s][Response][account_id:%s]", __METHOD__, $accountId),
                $accountLink->toArray()
            );
        }

        return $accountLink;
    }

    /**
     * Retrieve external bank account.
     *
     * {@inheritDoc}
     */
    public function retrieveExternalBankAccount(string $accountId, string $externalAccountId): BankAccount
    {
        if ($this->logger) {
            $this->logger->info(
                sprintf(
                    "[%s][Request][account_id:%s][bank_account_id:%s]",
                    __METHOD__,
                    $accountId,
                    $externalAccountId
                )
            );
        }

        $bankAccount = $this->handleException(function () use ($accountId, $externalAccountId) {
            return StripeAccount::retrieveExternalAccount(
                $accountId,
                $externalAccountId,
                null,
                $this->getStripeOptions()
            );
        });

        if ($this->logger) {
            $this->logger->info(
                sprintf(
                    "[%s][Response][account_id:%s][bank_account_id:%s]",
                    __METHOD__,
                    $accountId,
                    $externalAccountId
                ),
                $bankAccount->toArray()
            );
        }

        return $bankAccount;
    }

    /**
     * Create external bank account.
     *
     * {@inheritDoc}
     */
    public function createExternalBankAccount(
        string $accountId,
        array $bankAccountPayload,
        array $metadata = []
    ): BankAccount {
        $bankAccountPayload[self::OBJECT] = BankAccount::OBJECT_NAME;
        if (empty($bankAccountPayload[self::CURRENCY])) {
            $bankAccountPayload[self::CURRENCY] = self::DEFAULT_CURRENCY;
        }
        if (empty($bankAccountPayload[self::COUNTRY])) {
            $bankAccountPayload[self::COUNTRY] = self::DEFAULT_COUNTRY;
            $bankAccountPayload[self::CURRENCY] = self::DEFAULT_CURRENCY;
        }
        $payload[AccountService::EXTERNAL_ACCOUNT] = $bankAccountPayload;
        $payload[AccountService::METADATA] = $this->attachAdditionalMetadata($metadata);

        if ($this->logger) {
            $this->logger->info(sprintf("[%s][Request][account_id:%s]", __METHOD__, $accountId), $payload);
        }

        /* @var BankAccount $payload */
        $bankAccount = $this->handleException(function () use ($accountId, $payload) {
            return StripeAccount::createExternalAccount($accountId, $payload, $this->getStripeOptions());
        });

        if ($this->logger) {
            $this->logger->info(
                sprintf(
                    "[%s][Response][account_id:%s][bank_account_id:%s]",
                    __METHOD__,
                    $accountId,
                    $bankAccount->id
                ),
                $bankAccount->toArray()
            );
        }

        return $bankAccount;
    }
}
