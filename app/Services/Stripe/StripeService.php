<?php

namespace App\Services\Stripe;

use App\Services\Stripe\Concerns\IStripeLogger;
use App\Services\Stripe\Concerns\IStripeService;
use Closure;
use Psr\Log\LoggerInterface;
use Stripe\Exception\ApiErrorException as StripeApiException;
use Stripe\Stripe;

/**
 * Base class for stripe api services.
 */
abstract class StripeService implements IStripeService
{
    /**
     * Stripe API key.
     *
     * @var string
     */
    protected $apiKey;

    /**
     * Stripe API version.
     *
     * @var string
     */
    protected $apiVersion;

    /**
     * Logger.
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Additional metadata.
     *
     * @var mixed[]|array
     */
    protected $additionalMetadata;

    /**
     * StripeService constructor.
     *
     * @param IStripeLogger|null $logger Logger
     */
    public function __construct(?IStripeLogger $logger = null)
    {
        $this->setApiKey(Stripe::getApiKey());
        $this->setApiVersion(Stripe::getApiVersion());
        if ($logger) {
            $this->setLogger($logger);
        }
    }

    /**
     * Return the API key used for requests.
     *
     * {@inheritDoc}
     */
    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    /**
     * Set API key.
     *
     * {@inheritDoc}
     */
    public function setApiKey(?string $apiKey = null): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Returns the API version used for requests, null if we're using the latest version.
     *
     * {@inheritDoc}
     */
    public function getApiVersion(): ?string
    {
        return $this->apiVersion;
    }

    /**
     * Set API version.
     *
     * {@inheritDoc}
     */
    public function setApiVersion(?string $apiVersion = null): void
    {
        $this->apiVersion = $apiVersion;
    }

    /**
     * Get logger.
     *
     * {@inheritDoc}
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    /**
     * Set logger.
     *
     * {@inheritDoc}
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * Get Stripe options.
     *
     * {@inheritDoc}
     */
    public function getStripeOptions(): array
    {
        $options = [];
        if ($this->apiKey) {
            $options['api_key'] = $this->apiKey;
        }
        if ($this->apiVersion) {
            $options['stripe_version'] = $this->apiVersion;
        }

        return $options;
    }

    /**
     * Returns additional metadata (that will be added automatically to Stripe requests).
     *
     * @return mixed[]|array
     */
    public function getAdditionalMetadata(): array
    {
        if (is_null($this->additionalMetadata)) {
            $collect = collect((array) config('stripe.additional_metadata'));
            $this->additionalMetadata = $collect->filter(function ($value) {
                return !empty($value);
            })->all();
        }

        return $this->additionalMetadata;
    }

    /**
     * Attach additional metadata to current metadata.
     *
     * @param array $metadata Metadata
     *
     * @return mixed[]|array
     */
    protected function attachAdditionalMetadata(array $metadata): array
    {
        return array_merge($this->getAdditionalMetadata(), $metadata);
    }

    /**
     * Handle exception.
     *
     * @param Closure $closure Closure
     *
     * @return mixed
     *
     * @throws StripeApiException
     */
    protected function handleException(Closure $closure)
    {
        try {
            return $closure();
        } catch (StripeApiException $e) {
            if ($this->logger) {
                $this->logger->error($e->getMessage(), (array) $e->getJsonBody());
                $this->logger->error($e->getTraceAsString());
            }
            throw $e;
        }
    }
}
