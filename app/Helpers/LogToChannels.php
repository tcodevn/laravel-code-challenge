<?php
/**
 * Logger helper to log into different files.
 *
 */

namespace App\Helpers;

use Exception;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

/**
 * Class LogToChannels
 *
 * @package App\Helpers
 */
class LogToChannels
{
    /**
     * Shared Instance.
     *
     * @var LogToChannels
     */
    private static $sharedInstance = null;

    /**
     * The LogToChannels channels.
     *
     * @var Logger[]
     */
    protected $channels = [];

    /**
     * Generic log function.
     *
     * @param string $logFile The log file name name
     * @param string $channel The channel to log the record in
     * @param int $level The error level
     * @param string $message The error message
     * @param array $context Optional context arguments
     *
     * @return boolean Whether the record has been processed
     */
    public function log(string $logFile, string $channel, int $level, string $message, array $context = []): bool
    {
        try {
            // Try to add the logger if doesn't exist
            if (!isset($this->channels[$logFile])) {
                $handler = new StreamHandler($this->getChannelPath($logFile));
                $handler->setFormatter(new LineFormatter(null, null, true, true));

                $this->addChannel($logFile, $channel, $handler);
            }
            /* @var Logger $log */
            $log = $this->channels[$logFile] ?? null;
            if (!empty($channel) && $channel != '_') {
                $log = $log->withName($channel);
            }

            // LogToChannels the record
            return $log->{Logger::getLevelName($level)}($message, $context) == null ?? false;
        } catch (Exception $ex) {
            if ($channel === 'testing') {
                throw $ex;
            }
            echo($ex->getMessage());
            return false;
        }
    }

    /**
     * Add a channel to log in
     *
     * @param string $logFile The log file name
     * @param string $channelName The channel name
     * @param HandlerInterface $handler The channel handler
     * @param string|null $path The path of the channel file, DEFAULT storage_path()/logs
     *
     * @throws Exception When the channel already exists
     */
    protected function addChannel(string $logFile, string $channelName, HandlerInterface $handler, ?string $path = null): void
    {
        if (isset($this->channels[$logFile])) {
            throw new Exception("The log file channel already exists `{$logFile}`");
        }

        $this->channels[$logFile] = new Logger($channelName);
        $this->channels[$logFile]->pushHandler(
            new $handler(
                $path === null ? $this->getChannelPath($logFile) : $path . DIRECTORY_SEPARATOR . $logFile . '.log'
            )
        );
    }

    /**
     * Adds a log record at the DEBUG level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function debug(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::DEBUG, $message, $context);
    }

    /**
     * Adds a log record at the INFO level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function info(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::INFO, $message, $context);
    }

    /**
     * Adds a log record at the NOTICE level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function notice(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::NOTICE, $message, $context);
    }

    /**
     * Adds a log record at the WARNING level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function warn(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::WARNING, $message, $context);
    }

    /**
     * Adds a log record at the WARNING level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function warning(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::WARNING, $message, $context);
    }

    /**
     * Adds a log record at the ERROR level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function err(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::ERROR, $message, $context);
    }

    /**
     * Adds a log record at the ERROR level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function error(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::ERROR, $message, $context);
    }

    /**
     * Adds a log record at the CRITICAL level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function crit(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::CRITICAL, $message, $context);
    }

    /**
     * Adds a log record at the CRITICAL level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function critical(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::CRITICAL, $message, $context);
    }

    /**
     * Adds a log record at the ALERT level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function alert(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::ALERT, $message, $context);
    }

    /**
     * Adds a log record at the EMERGENCY level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function emerg(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::EMERGENCY, $message, $context);
    }

    /**
     * Adds a log record at the EMERGENCY level.
     *
     * @param string $message The log message
     * @param string $logFile The log file name
     * @param array $context The log context
     * @param string $channel The channel name
     *
     * @return boolean Whether the record has been processed
     */
    public function emergency(string $message, string $logFile = 'LogToChannels', array $context = [], string $channel = '_'): bool
    {
        return $this->log($logFile, $channel, Logger::EMERGENCY, $message, $context);
    }

    /**
     * Get full path of log file.
     *
     * @param string $logFile File log string
     *
     * @return string
     */
    public function getChannelPath(string $logFile = 'LogToChannels'): string
    {
        return storage_path() . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logFile . '.log';
    }

    /**
     * Read log file
     *
     * @param string $logFile File log name
     *
     * @return string|null
     */
    public function getLogContent(string $logFile = 'LogToChannels'): ?string
    {
        $filePath = $this->getChannelPath($logFile);

        if (is_file($filePath)) {
            return file_get_contents($filePath);
        }

        return null;
    }

    /**
     * Delete log file by channel name.
     *
     * @param string $logFile File log name
     */
    public function deleteLog(string $logFile = 'LogToChannels'): void
    {
        $filePath = $this->getChannelPath($logFile);
        if (is_file($filePath)) {
            unlink($filePath);
        }
    }

    /**
     * Get Instance
     */
    public static function getInstance() : LogToChannels
    {
        if (!self::$sharedInstance) {
            self::$sharedInstance = new LogToChannels();
        }
        return self::$sharedInstance;
    }
}
