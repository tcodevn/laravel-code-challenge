<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Repositories\CustomerRepository;
use App\Models\User;
use Illuminate\Http\Response;
use Faker\Factory as Faker;

class UserCustomerTest extends TestCase
{
    use JwtTokenTesting;

    protected $customerRepository;
    protected $customer;

    public function setUp() : void
    {
        parent::setUp();
        $this->faker = Faker::create();

        $this->customerRepository = app(CustomerRepository::class);

        $this->customer = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail(),
            'description' => $this->faker->name,
        ];
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testCreateCustomerRequireName()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $params = [
            'email'     => 'john@gmail.com'
        ];

        $response = $this->post(route('customer.store'), $params);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testCreateCustomerRequireEmail()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $params = [
            'name' => "Peter John",
        ];

        $response = $this->post(route('customer.store'), $params);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testCreateCustomerWithInvalidEmail()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $params = [
            'name' => "Peter John",
            'email' => 'invalidEmail'
        ];

        $response = $this->post(route('customer.store'), $params);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testCreateCustomerWithInvalidName()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $params = [
            'name'  => 123,
            'email'      => 'john@gmail.com'
        ];

        $response = $this->post(route('customer.store'), $params);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testStore()
    {
        $customer = $this->customerRepository->create($this->customer);

        $this->assertInstanceOf(\Stripe\Customer::class, $customer);

        $this->assertEquals($this->customer['name'], $customer['name']);
        $this->assertEquals($this->customer['email'], $customer['email']);
        $this->assertEquals($this->customer['description'], $customer['description']);
    }

    public function testGet()
    {
        $inputCustomer = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail(),
            'description' => $this->faker->name,
        ];

        $customer = $this->customerRepository->create($inputCustomer);
        $createdCustomer = $this->customerRepository->get($customer->id);

        $this->assertInstanceOf(\Stripe\Customer::class, $createdCustomer);

        $this->assertEquals($createdCustomer['name'], $customer['name']);
        $this->assertEquals($createdCustomer['email'], $customer['email']);
        $this->assertEquals($createdCustomer['description'], $customer['description']);
    }

    public function testUpdate()
    {
        $inputCustomer = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail(),
            'description' => $this->faker->name,
        ];

        $customer = $this->customerRepository->create($inputCustomer);

        $updatedCustomer = $this->customerRepository->update($customer->id, $this->customer);

        $this->assertInstanceOf(\Stripe\Customer::class, $updatedCustomer);

        $this->assertEquals($updatedCustomer['name'], $this->customer['name']);
        $this->assertEquals($updatedCustomer['email'], $this->customer['email']);
        $this->assertEquals($updatedCustomer['description'], $this->customer['description']);
    }
}
